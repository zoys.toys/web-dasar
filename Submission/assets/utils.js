let navLinks = document.getElementById("navLinks");

function showMenu(){
    navLinks.style.right = "0";
}

function hideMenu(){
    navLinks.style.right = "-200px";
}


var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;

function stickF() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}

window.onscroll = function(){
    stickF();
};

const sections = document.querySelectorAll("section");
const nav = document.querySelectorAll("nav div.nav-links ul li");

window.addEventListener("scroll", function(){
    let current = "";
    for(let section of sections){
        const sectionTop = section.offsetTop;
        const sectionHeight = section.clientHeight;
        if(pageYOffset >= sectionTop - sectionHeight / 3){
            current = section.getAttribute("id");
        }
    }

    for(let li of nav){
        li.classList.remove("active");
        if(li.classList.contains(current)){
            li.classList.add("active");
        }
    }
    
});